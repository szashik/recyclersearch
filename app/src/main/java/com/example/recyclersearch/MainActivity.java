package com.example.recyclersearch;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btn_add, btn_remove;
    int l_track = 0;
    int r_track = 0;

    LinearLayout add_to_Layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        add_to_Layout  = (LinearLayout) findViewById(R.id.inflatedLayout);

        btn_add = (Button) findViewById(R.id.addButton);
        btn_add.setOnClickListener(this);
        btn_remove = (Button) findViewById(R.id.removeButton);
        btn_remove.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addButton:
                LayoutInflater l = getLayoutInflater();
                View addedLayout = l.inflate(R.layout.listed_item, add_to_Layout, false);
                TextView l_text = (TextView) addedLayout.findViewById(R.id.leftText);
                l_text.setText("Left Value: " + (++l_track));
                TextView r_text = (TextView) addedLayout.findViewById(R.id.rightText);
                r_text.setText("Right value: " + (++r_track));
                add_to_Layout.addView(addedLayout);
                break;
        }
    }
}
